getDatatable();
setToastr();

function getDatatable() {
  $.ajax({
    url: '/blog/list',
    type: 'POST',
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
  })
    .done(function (res) {
      $('#data-table').html(res);
    })
    .fail(function () {
      console.error('blog/list post');
    })
    .always(function () {
      setTimeout(() => {
        $('#spinner').addClass('d-none');
      }, 200);
    });
}

$(document).on('click','div[del]', function(e){
  swal.fire({
	  title: 'Delete this blog?',
	  text: "",
	  type: 'error',
	  showCancelButton: true,
	  confirmButtonText: 'Confirm',
	  cancelButtonText: 'Back'
	}).then((result) => {
	  if (result.value) {
	  	const id = $(this).attr('del');
      $.ajax({
        url: '/blog/' + id,
        type: 'DELETE',
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
      })
        .done(function (res) {
          if (res == 204) {
            setTimeout(() => {
              $('#spinner').removeClass('d-none');
            }, 100);
            setTimeout(() => {
              toastr.info("Delete blog complete");
            }, 200);
            
            getDatatable();
          }
        })
        .fail(function () {
          console.error('article delete id#' + id);
        })
        .always(function () {
          
        });
	  }
	})
});