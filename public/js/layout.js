/*!
 * Hoangnqph06760@fpt.edu.vn
 * Date: 2020-04-20
 */
setState();
getContent();

$.ajaxPrefilter(function (options, original_Options, jqXHR) {
	options.async = true;
});

function getContent() {
	$.ajax({
		url: '',
		type: 'POST',
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	})
		.done(function (res) {
			console.log("Navigated to " + window.location.href);
			$('#router-outlet').html(res);
			initActiveMenu();
		})
		.fail(function () {
			console.error("Error routing page");
		})
		.always(function () {
			preventSharp();
		});
}

$(document).on('click','a[routerLink]', function(e){
	let url = $(this).attr('routerLink');
	if (url) {
		e.preventDefault();
		navigate(url);

		removeActiveMenu();
		setActiveMenu($(this));
	}
});

function navigate(url) {
	history.pushState('data', 'title', url);
	getContent();
	window.scrollTo(0, 0);
}

function preventSharp() {
	setTimeout(() => {
		$('a').click(function (e) {
			if ($(this).attr('href') == '#') e.preventDefault();
		});
	}, 2000);
}

function setActiveMenu(menu) {
	menu.parent('li').addClass('active');
}

function removeActiveMenu() {
	$('ul.list-menu li.active').removeClass('active');
}

function initActiveMenu() {
	removeActiveMenu();
	$('ul.list-menu > li > a').each(function (index, el) {
		let routerLink = $(this).attr('routerLink');
		var pattID = new RegExp(routerLink + "/([0-9]+)$");
		let pathname = window.location.pathname;
		if (pathname == '/') pathname = '/blog';
		if (pathname == routerLink || pattID.test(pathname)) {
			setActiveMenu($(this));
			return false;
		}
	});
}

function setState() {
	jQuery(document).ready(function($) {
		if (window.history && window.history.pushState) {
			$(window).on('popstate', function() {
				getContent();
			});
		}
	});
}