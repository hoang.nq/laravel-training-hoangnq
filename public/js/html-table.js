
// Class definition

var KTDatatableHtmlTableDemo = function() {
	// Private functions

	// demo initializer
	var demo = function() {

		var datatable = $('.kt-datatable').KTDatatable({
			data: {
				saveState: {cookie: false},
			},
			search: {
				input: $('#generalSearch'),
      },
      rows: {
        autoHide: true,
      },
			columns: [
				{
          field: 'ID',
          type: 'number',
          width: 50,
				},
				{
					field: 'Title',
					autoHide: false,
        },
				{
					field: 'Content',
          width: 350,
					autoHide: false,
        }, 
        {
					field: 'Status',
					title: 'Status',
          width: 90,
					// callback function support for column rendering
					template: function(row) {
            // console.log(row)
						var status = {
							0: {'title': 'Hidden', 'class': 'kt-badge--unified-dark'},
							1: {'title': 'Public', 'class': ' kt-badge--success'},
						};
						return '<span class="kt-badge ' + status[row.Status].class + ' kt-badge--inline kt-badge--pill">' + status[row.Status].title + '</span>';
					},
        },
        {
					field: 'Category',
          width: 120,
        },
        {
					field: 'Public at',
          width: 90,
        },
        {
					field: 'Edit',
					title: 'Edit',
					autoHide: true,
          width: 70,
          sortable: false,
        }, 
        {
					field: 'Delete',
					title: 'Delete',
					autoHide: true,
          width: 80,
          sortable: false,
				},
			],
		});

    $('#kt_form_status').on('change', function() {
      datatable.search($(this).val().toLowerCase(), 'Status');
    });

    $('#kt_form_type').on('change', function() {
      datatable.search($(this).val().toLowerCase(), 'Type');
    });

    $('#kt_form_status,#kt_form_type').selectpicker();

	};

	return {
		// Public functions
		init: function() {
			// init dmeo
			demo();
		},
	};
}();

jQuery(document).ready(function() {
	KTDatatableHtmlTableDemo.init();
});

