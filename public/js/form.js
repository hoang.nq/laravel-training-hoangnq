$('#main_form').submit(function(event) {
  event.preventDefault();

  if ((this).checkValidity()) {
    const isUpdate = $('#id').val() != null && $('#id').val().trim().length > 0
    $.ajax({
      url: isUpdate ? '' : 'new',
      type: 'PUT',
      dataType: 'json',
      data: $('form#main_form').serialize()
    })
      .done(function (res) {
        if (res == 201) {
          navigate('/blog');
          initActiveMenu();
          setTimeout(() => {
            const msg = isUpdate ? 'Update blog successfully' : 'Create blog successfully'
            toastr.success(msg);
          }, 500);
        }
      })
      .fail(function () {
        console.error('#main_form submit');
      })
      .always(function () {
        
      });
  } else {
    $(this).addClass('was-validated');
  }
});

$("#btn-reset").click(function() {
  $(':input','#main_form')
    .not(':button, :submit, :reset, :hidden')
    .val('')
  $("#category").val(0);
  $("#r-public").prop("checked", true);
});

$("#btn-back").click(function() {
  window.history.back();
});
