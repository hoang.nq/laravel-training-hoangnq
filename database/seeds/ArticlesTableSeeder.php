<?php

use Illuminate\Database\Seeder;
use App\Article;

class ArticlesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Let's truncate our existing records to start from scratch.
        Article::truncate();

        $faker = \Faker\Factory::create();

        // And now, let's create a few articles in our database:
        for ($i = 0; $i < 50; $i++) {
            Article::create([
                'title' => $faker->sentence,
                'content' => $faker->paragraph,
                'status' => $faker->numberBetween(0, 1) == 1 ? 1 : $faker->numberBetween(0, 1),
                'category_id' => $faker->numberBetween(1, 11),
            ]);
        }
    }
}
