<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Let's truncate our existing records to start from scratch.
        Category::truncate();

        $faker = \Faker\Factory::create();

        // And now, let's create a few articles in our database:
        Category::create([
            'name' => 'World',
        ]);
        Category::create([
            'name' => 'Entertainment',
        ]);
        Category::create([
            'name' => 'Sport',
        ]);
        Category::create([
            'name' => 'Business',
        ]);
        Category::create([
            'name' => 'Technology',
        ]);
        Category::create([
            'name' => 'Lifestyle',
        ]);
        Category::create([
            'name' => 'Travel',
        ]);
        Category::create([
            'name' => 'Film',
        ]);
        Category::create([
            'name' => 'Government',
        ]);
        Category::create([
            'name' => 'Medical',
        ]);
        Category::create([
            'name' => 'Science',
        ]);
    }
}
