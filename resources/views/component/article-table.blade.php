<table class="kt-datatable" id="html_table">
  <thead>
    <tr>
      <th title="Field #1" scope="col">
        ID
      </th>
      <th title="Field #2" scope="col">
        Title
      </th>
      <th title="Field #3" scope="col">
        Content
      </th>
      <th title="Field #4" scope="col">
        Category
      </th>
      <th title="Field #5" scope="col">
        Status
      </th>
      <th title="Field #6" scope="col">
        Public at
      </th>
      <th title="Field #7" scope="col">
        Edit
      </th>
      <th title="Field #8" scope="col">
        Delete
      </th>
    </tr>
  </thead>
  <tbody>
      @foreach ($data as $article)
        <tr>
            <td> {{ $article['id'] }} </td>
            <td> {{ $article['title'] }} </td>
            <td> {{ $article['content'] }} </td>
            <td> {{ $article['category']['name'] }} </td>
            <td> {{ $article['status'] }} </td>
            <td> {{ $article['created_at'] }} </td>
            <td>
                <a class="btn-table" routerLink="/blog/{{ $article['id'] }}"><i class="fas fa-edit"></i> Edit </a>
            </td>
            <td>
                <div class="btn-table" del="{{ $article['id'] }}"><i class="fas fa-trash-alt"></i> Delete </div>
            </td>
        </tr>
      @endforeach
  </tbody>
</table>

<script src="/js/html-table.js" type="text/javascript"></script>
