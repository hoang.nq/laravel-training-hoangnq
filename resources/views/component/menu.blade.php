<div id="AppMenu">
  <br><br><br>
  <ul class="list-menu">
    <li>
      <a routerLink="/blog">
        <div><i class="fas fa-list"></i> List</div>
      </a>
    </li>
    <li>
      <a routerLink="/blog/new">
        <div><i class="fas fa-plus"></i> New</div>
      </a>
    </li>
    <!-- <li>
      <a href="/edit">
        <div>Edit</div>
      </a>
    </li> -->
  </ul>
</div>
