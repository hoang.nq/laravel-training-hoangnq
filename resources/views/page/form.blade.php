
<div class="min-vh-100">
  <form id="main_form" action="" novalidate>
    @csrf
    <div class="form-group">
      <label for="title">ID</label>
      <input id="id" name="title" type="text" class="form-control" value="{{ $article['id'] }}" disabled>
    </div>

    <div class="form-group">
      <label for="title">Title</label>
      <input id="title" name="title" type="text" class="form-control" value="{{ $article['title'] }}" autocomplete="off" required>
      <div class="invalid-feedback">
        Title required
      </div>
    </div>

    <div class="form-group col-3 p-0">
      <label for="category">Category</label>
      <select id="category" name="category_id" class="form-control" required>
        <option value="">Please select</option>
        @foreach ($categories as $category)
          <option 
            value="{{ $category['id'] }}" 
            <?php print $article['category_id'] == $category['id'] ? 'selected' : '' ?>
          >
            {{ $category['name'] }}
          </option>
        @endforeach
      </select>
      <div class="invalid-feedback">
        Category required
      </div>
    </div>

    <div class="form-group">
      <label>Status</label>
      <div class="form-check">
        <input
          id="r-public"
          class="form-check-input"
          type="radio"
          name="status"
          value="1"
          <?php print $article['status'] === 1 || $article['status'] === null ? 'checked' : '' ?>
        >
        <label class="form-check-label" for="r-public">
          Public
        </label>
      </div>
      <div class="form-check">
        <input
          id="r-hidden"
          class="form-check-input"
          type="radio"
          name="status"
          value="0"
          <?php print $article['status'] === 0 ? 'checked' : '' ?>
        >
        <label class="form-check-label" for="r-hidden">
          Hidden
        </label>
      </div>
    </div>
    
    <div class="form-group">
      <label for="detail">Content</label>
      <textarea id="detail" name="content" class="form-control" rows="7" required>{{ $article['content'] }}</textarea>
      <div class="invalid-feedback">
        Content required
      </div>
    </div>

    <!-- <div class="form-group">
      <label for="image">Hình ảnh</label>
      <input id="image" type="file" class="form-control-file">
    </div> -->

    <!-- <div class="form-group">
      <label for="data_pubblic">Date Public</label>
      <input id="data_pubblic" type="date" class="form-control">
    </div> -->

    <div class="form-group text-center">
      <button type="submit" class="btn btn-success">
        Submit
      </button>
      <button id="btn-back" type="button" class="btn btn-primary">
        Back
      </button>
    </div>
  </form>
</div>

<script src="/js/form.js" type="text/javascript"></script>