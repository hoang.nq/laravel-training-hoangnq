<div class="min-vh-100">
    <h5>Search Blogs</h5>
    <form>
        <div class="form-group row">
          <label for="search" class="col-sm-2 col-form-label">Search</label>
          <div class="col-sm-10">
            <div class="kt-input-icon kt-input-icon--left">
                <input type="text" class="form-control" placeholder="Search..." id="generalSearch" autocomplete="off">
                <span class="kt-input-icon__icon kt-input-icon__icon--left">
                    <span><i class="la la-search"></i></span>
                </span>
            </div>
            <button type="submit" class="btn btn-success mt-3 d-none">
              Search
            </button>
          </div>
        </div>
    </form>

    <h5>List Blogs</h5>
    <div class="row align-items-center mt-4">
        <div style="" class="col-md-3 kt-margin-b-20-tablet-and-mobile md-4">
            <div class="kt-form__group kt-form__group--inline row">
                <div class="col-3 h-center h6">
                    Status:
                </div>
                <div class="col-9">
                    <div class="kt-form__control">
                        <select class="form-control bootstrap-select kt_form_method" id="kt_form_status">
                            <option value="">All</option>
                            <option value="1">Public</option>
                            <option value="0">Hidden</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="text-center mt-3">
      <div id="spinner" class="spinner-border text-primary" role="status">
        <span class="sr-only">Loading...</span>
      </div>
    </div>
    <div id="data-table"></div>
</div>
<script src="/js/list.js" type="text/javascript"></script>

