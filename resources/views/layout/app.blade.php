<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Hoangnq</title>
        <link rel="stylesheet" type="text/css" href="/css/plugins.bundle.css">
	    <link rel="stylesheet" type="text/css" href="/css/style.bundle.css">
        <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
        <link rel="stylesheet" href="/css/toastr.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/all.min.css">
    </head>
    <body>
        @include("component.menu")
        <div id="AppPage" class="bg-white">
            @include("component.header")
                <div id="router-outlet"></div>
            @include("component.footer")
        </div>

        <script src="/js/config.js" type="text/javascript"></script>
        <script src="/js/toastr.min.js" type="text/javascript"></script>
        <script src="/js/plugins.bundle.js" type="text/javascript"></script>
        <script src="/js/scripts.bundle.js" type="text/javascript"></script>
        <script src="/js/layout.js" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/js/all.min.js" type="text/javascript"></script>
    </body>
</html>
