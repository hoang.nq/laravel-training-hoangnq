<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/welcome/{any}', function () {
    return view('page.welcome');
});

Route::post('/', 'Web\BlogController@blogListLayout');
Route::post('/blog', 'Web\BlogController@blogListLayout');
Route::post('/blog/list', 'Web\BlogController@list');
Route::get('/blog/{any}', 'Web\BlogController@layout');
Route::post('/blog/new', 'Web\BlogController@form');
Route::put('/blog/new', 'Web\BlogController@store');
Route::post('/blog/{article}', 'Web\BlogController@form');
Route::put('/blog/{article}', 'Web\BlogController@update');
Route::delete('/blog/{article}', 'Web\BlogController@delete');
Route::get('/{any}', 'Web\BlogController@layout');
Route::get('/', 'Web\BlogController@layout');

