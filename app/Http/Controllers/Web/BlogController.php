<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Article;
use App\Category;
use App\Http\Controllers;
use App\Http\Controllers\Controller;

class BlogController extends Controller
{
    public function list()
    {
        $data = Article::all()->load(['category']);
        return view('component.article-table')
            ->withData($data);
    }

    public function form(Article $article)
    {
        return view('page.form')
            ->withArticle($article)
            ->withCategories(Category::all());
    }

    public function store(Request $request)
    {   
        $article = Article::create($request->all());

        return '201';
    }

    public function update(Request $request, Article $article)
    {
        $article->update($request->all());

        // return redirect('/blog');
        return '201';
    }

    public function delete(Article $article)
    {
        $article->delete();

        return '204';
    }

    public function layout()
    {
        return view('layout.app');
    }

    public function blogListLayout()
    {
        return view('page.list');
    }

}

